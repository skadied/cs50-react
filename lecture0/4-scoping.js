//we can call functions that are defined later
thisIsHouisted()

thisIsAGlobalVariable = "hello"

//const value cannot change
const thisIsAConst = 50

//thisIsAConst++ //console.error

const constObj = {}

constObj.a = 'a'

//ends at the next bracket
let thisIsALet = 51
thisIsALet = 51
//let thisIsALet = 51//errors!

//var can reassign a value
//overwrite or shadow
var thisIsAVar = 50
thisIsAVar = 50
var thisIsAVar = 'new value!'

console.log(thisIsAVar)

function thisIsHouisted() {
  console.log('this is a function declared at the bottom of a file')
}

const thisIsNotHoisted = function() {
  console.log('should this be hoisted?')
}

console.log(global)
